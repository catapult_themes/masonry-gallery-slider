<?php
/**
 * Functions and data for the admin
 * Includes our settings
 */

// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Returns an array of settings for the Gallery tab
 *
 * @since 1.0.0
 * @return Array
 */
if( ! function_exists( 'sliderify_gallery_page_settings' ) ) {
	function sliderify_gallery_page_settings() {
		$settings = array(
			'link_to' => array(
				'id'		=> 'link_to',
				'label'		=> __( 'Link To', 'sliderify' ),
				'callback'	=> 'select_callback',
				'choices'	=> array(
					'post'	=> __( 'Attachment Page', 'sliderify' ),
					'file'	=> __( 'Media File', 'sliderify' ),
					'none'	=> __( 'None', 'sliderify' )
				),
				'description' => __( 'This setting will be overridden if you have the Lightbox Slider enabled', 'sliderify' ),
				'page'		=> 'sliderify_gallery',
				'section'	=> 'ctmgs_gallery_settings',
			),
			'columns' => array(
				'id'		=> 'columns',
				'label'		=> __( 'Columns', 'sliderify' ),
				'callback'	=> 'select_callback',
				'choices'	=> array(
					'2'		=> '2',
					'3'		=> '3',
					'4'		=> '4',
					'5'		=> '5',
					'6'		=> '6',
					'7'		=> '7',
					'8'		=> '8',
					'9'		=> '9',
				),
				'description' => __( 'Set default number of columns for Masonry Gallery', 'sliderify' ),
				'page'		=> 'sliderify_gallery',
				'section'	=> 'ctmgs_gallery_settings',
			),
			'size' => array(
				'id'		=> 'size',
				'label'		=> __( 'Size', 'sliderify' ),
				'callback'	=> 'select_callback',
				'choices'	=> array(
					'none'			=> __( 'No Default', 'sliderify' ),
					'thumbnail'		=> __( 'Thumbnail', 'sliderify' ),
					'medium'		=> __( 'Medium', 'sliderify' ),
					'large'			=> __( 'Large', 'sliderify' ),
					'full'			=> __( 'Full', 'sliderify' ),
				),
				'description'	=> __( 'Unlike the Link To and Columns settings above, a default value cannot be attributed to the Size setting from the admin side (i.e. when you create a new gallery). Instead, if you specify a value here, all galleries in your site will use this value for gallery image sizes.', 'sliderify' ),
				'page'		=> 'sliderify_gallery',
				'section'	=> 'ctmgs_gallery_settings',
			),
			'landscape' => array(
				'id'		=> 'landscape',
				'label'		=> __( 'Landscape / Portrait', 'sliderify' ),
				'callback'	=> 'checkbox_callback',
				'description'	=> __( 'Enable this to set landscape and portrait images to different widths. Works best with lots of images and several columns. For the masonry gallery only.', 'sliderify' ),
				'page'		=> 'sliderify_gallery',
				'section'	=> 'ctmgs_gallery_settings',
			),
			'existing_gallery_format' => array(
				'id'		=> 'existing_gallery_format',
				'label'		=> __( 'Existing Gallery Format', 'sliderify' ),
				'callback'	=> 'select_callback',
				'choices'	=> array(
					'default'	=> __( 'Default', 'sliderify' ),
					'masonry'	=> __( 'Masonry', 'sliderify' ),
					'slider'	=> __( 'Slider', 'sliderify' )
				),
				'description' => __( 'This setting will apply to existing galleries created before you activated this plugin. Default will mean that existing galleries are not updated; otherwise, choosing Masonry or Slider will convert all existing galleries to either masonry or slider layouts.', 'sliderify' ),
				'page'		=> 'sliderify_gallery',
				'section'	=> 'ctmgs_gallery_settings',
			),
		);
		
		return $settings;
	}

}

/**
 * Returns an array of settings for the Gallery Slider tab
 *
 * @since 1.0.0
 * @return Array
 */
if( ! function_exists( 'sliderify_slider_page_settings' ) ) {
	function sliderify_slider_page_settings() {
		$settings = array(
			'slider_margin' => array(
				'id'		=> 'slider_margin',
				'label'		=> __( 'Margin', 'sliderify' ),
				'callback'	=> 'text_callback',
				'description' => __( 'Margin between images in slider.', 'sliderify' ),
				'page'		=> 'sliderify_slider',
				'section'	=> 'ctmgs_slider_settings',
			),
		);
		
		$settings = apply_filters( 'sliderify_filter_slider_page_settings', $settings );
		return $settings;
	}

}

/**
 * Returns an array of settings for the Lightbox Slider tab
 *
 * @since 1.0.0
 * @return Array
 */
if( ! function_exists( 'sliderify_lightbox_page_settings' ) ) {
	function sliderify_lightbox_page_settings() {
		$settings = array(
			'enable_slider' => array(
				'id'		=> 'enable_slider',
				'label'		=> __( 'Enable Lightbox Slider', 'sliderify' ),
				'callback'	=> 'checkbox_callback',
				'description' => __( 'Add lightbox slider when user clicks a masonry gallery image.', 'sliderify' ),
				'page'		=> 'sliderify_lightbox',
				'section'	=> 'ctmgs_lightbox_settings',
			),
		);
		
		return $settings;
	}

}