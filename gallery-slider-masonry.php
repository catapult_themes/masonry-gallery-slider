<?php
/*
Plugin Name: Sliderify
Description: Turn galleries into sliders
Version: 1.1.6
Author: Catapult Themes
Author URI: https://catapultthemes.com/
Text Domain: sliderify
Domain Path: /languages
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function sliderify_load_plugin_textdomain() {
    load_plugin_textdomain( 'sliderify', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'sliderify_load_plugin_textdomain' );

/**
 * Define constants
 **/
if ( ! defined( 'SLIDERIFY_PLUGIN_URL' ) ) {
	define( 'SLIDERIFY_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}
if ( ! defined( 'SLIDERIFY_PLUGIN_VERSION' ) ) {
	define( 'SLIDERIFY_PLUGIN_VERSION', '1.1.6' );
}

if ( is_admin() ) {
	require_once dirname( __FILE__ ) . '/admin/admin-settings.php';
	require_once dirname( __FILE__ ) . '/admin/class-sliderify-admin.php';
	require_once dirname( __FILE__ ) . '/admin/gallery-settings.php';
}

require_once dirname( __FILE__ ) . '/inc/functions.php';
require_once dirname( __FILE__ ) . '/inc/functions-gallery.php';
require_once dirname( __FILE__ ) . '/inc/functions-slider.php';


/**
 * This function runs when WordPress completes its upgrade process
 * It iterates through each plugin updated to see if ours is included
 * @param	$upgrader_object	Array
 * @param 	$options 			Array
 * Or 
 */
function sliderify_upgrade_completed( $upgrader_object, $options ) {
	// The path to our plugin's main file
	$our_plugin = plugin_basename( __FILE__ );
	// If an update has taken place and the updated type is plugins and the plugins element exists
	if( $options['action'] == 'update' && $options['type'] == 'plugin' && isset( $options['plugins'] ) ) {
		// Iterate through the plugins being updated and check if ours is there
		foreach( $options['plugins'] as $plugin ) {
			if( $plugin == $our_plugin ) {
				// Set a transient to record that our plugin has just been updated
				set_transient( 'sliderify_updated', 1 );
			}
		}
	}
}
add_action( 'upgrader_process_complete', 'sliderify_upgrade_completed', 10, 2 );

/**
 * Show a notice to anyone who has just updated this plugin
 * This notice shouldn't display to anyone who has just installed the plugin for the first time
 */
function sliderify_display_update_notice() {
	// Check the transient to see if we've just updated the plugin
	if( get_transient( 'sliderify_updated' ) ) {
		echo '<div class="notice notice-success"><p>' . __( 'Thanks for updating. Please note that this plugin is now called <strong>Sliderify</strong>. You can find its settings in Settings > Sliderify.', 'sliderify' ) . '</p></div>';
		delete_transient( 'sliderify_updated' );
	}
}
add_action( 'admin_notices', 'sliderify_display_update_notice' );