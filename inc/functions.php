<?php
/**
 * Functions
 */

/**
 * Enqueue scripts and styles.
 */
function sliderify_scripts() {
	//if( sliderify_is_lightbox_slider_enabled() ) {
		wp_enqueue_style( 'owl-style', SLIDERIFY_PLUGIN_URL . 'assets/css/owl.carousel.min.css' );
		wp_enqueue_script( 'owl-script', SLIDERIFY_PLUGIN_URL . 'assets/js/owl.carousel.min.js', array( 'jquery' ) );
		//}
	wp_enqueue_script( 'jquery-masonry', '', array( 'jquery', 'imagesloaded' ) );
	wp_enqueue_style( 'sliderify-style', SLIDERIFY_PLUGIN_URL . 'assets/css/style.css' );
}
add_action( 'wp_enqueue_scripts', 'sliderify_scripts' );