<?php
/**
 * Slider functions
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Is the lightbox slider enabled?
 * @since 1.0.0
 * @return Boolean
 */
if( ! function_exists( 'sliderify_is_lightbox_slider_enabled' ) ) {
	function sliderify_is_lightbox_slider_enabled() {
		$options = get_option( 'ctmgs_lightbox_settings' );
		if( ! empty( $options['enable_slider'] ) ) {
			return true;
		}
		return false;
	}
}

/**
 * The markup for the gallery slider
 * @since 1.0.0
 * @param $images	Array of image IDs in gallery
 * @return HTML
 */
if( ! function_exists( 'sliderify_get_slider_gallery' ) ) {
	function sliderify_get_slider_gallery( $output='', $atts, $instance ) {

		// Filter the $atts in case you want to override some of the settings below
		$atts = apply_filters( 'sliderify_filter_slider_gallery_atts', $atts, $instance );
		
		// Link To
		$link_to = sliderify_get_link_setting( $atts );
	
		// Size
		$size = sliderify_get_default_size( $atts );
	
		if( ! isset( $atts['ids'] ) ) {
			return '';
		}
		
		// Get the image IDs
		$images = explode( ',', $atts['ids'] );
	
		// Gallery classes
		$classes = array( 'slider-gallery' );
		
		// Get some additional gallery settings
		$force_image_height = "false";
		$items = 1;
		if( isset( $atts['ctmgs_force_image_height'] ) ) {
			$force_image_height = $atts['ctmgs_force_image_height'];
		}
		if( isset( $atts['ctmgs_items'] ) ) {
			$items = $atts['ctmgs_items'];
		}
		
		$autowidth = "false";
		$autoheight = "true";
		if( isset( $atts['ctmgs_force_image_height'] ) && $force_image_height == 'true' ) {
			// If we're forcing the image height, we need to set number of items to 1 and enforce auto_width
			$items = 1;
			$autowidth = "true";
			$autoheight = "false";
			$classes[] = 'force-image-height';
		}
		
		// Filter the $classes
		$classes = apply_filters( 'sliderify_filter_slider_gallery_classes', $classes, $atts );

		$gallery = '';
	
		if( $images ) {
			// Use this to find the shortest height
			$heights = array();
			$count = 0;
			$gallery .= '<div class="slider-gallery-wrapper ">';
				$gallery .= '<div class="' . join( ' ', $classes ) . '">';
				$gallery .= '<div id="sliderify-carousel-' . esc_attr( $instance ) . '" class="owl-carousel">';
				if( ! empty( $images ) ) {
					foreach( $images as $image ) {
						$image_element = '';
						$image_attributes = wp_get_attachment_image_src( $image, $size );
						$image_element .= '<div class="sliderify-owl-item-inner">';
						$image_element .= '<img src="' . $image_attributes[0] . '">';
						// Add the height to our array
						if( $image_attributes[2] > 0 ) {
							$heights[] = $image_attributes[2];
						}
						$image_element .= '</div>';
						
						// Filter the image element
						$image_element = apply_filters( 'sliderify_filter_slider_image_element', $image_element, $image, $size, $atts );
						$gallery .= $image_element;
					}
				}
				$gallery .= '</div><!-- .owl-carousel -->';
				$gallery .= '</div><!-- .slider-gallery -->';
			$gallery .= '</div><!-- .slider-gallery-wrapper -->';
			
			// We'll set the carousel to this height
			$min_height = min( $heights );
			
			// Get some global settings
			$slider_options = get_option( 'ctmgs_slider_settings' );
			$margin = 0;
			if( ! empty( $slider_options['slider_margin'] ) ) {
				$margin = $slider_options['slider_margin'];
			}
			
			$carousel_params = sliderify_get_slider_carousel_params( $items, $margin, $autowidth, $autoheight, $atts, $instance );
	
			$gallery .= "<script>
			jQuery(document).ready(function($){
				carousel" . esc_attr( $instance ) . " = $('#sliderify-carousel-" . esc_attr( $instance ) . "');\n";
				if( $force_image_height == 'true' ) {
					$gallery .= "carousel" . esc_attr( $instance ) . ".css('height'," . esc_attr( $min_height ) . ");\n";
				}
				$gallery .= "carousel" . esc_attr( $instance ) . ".imagesLoaded(function(){";
					$gallery .= "carousel" . esc_attr( $instance ) . ".owlCarousel({";
					if( $carousel_params && is_array( $carousel_params ) ) {
						foreach( $carousel_params as $key=>$value ) {
							$gallery .= $key . ":" . $value . ",\n";
						}
					}
					$gallery .= "});";
				$gallery .= "});";
				// Allows us to add some extra script if required
				$gallery = apply_filters( 'sliderify_filter_additional_script', $gallery );
			$gallery .= "\n});
			</script>";
		}
	
		return $gallery;
		
	}
}

/**
 * The markup for the lightbox slider
 * @since 1.0.0
 * @param $images	Array of image IDs in gallery
 * @param $instance	Gallery ID
 * @return HTML
 */
if( ! function_exists( 'sliderify_get_lightbox_slider' ) ) {
	function sliderify_get_lightbox_slider( $images, $instance ) {
		$slider_html = '<div class="sliderify-slider-background sliderify-slider-close"></div>';
		$slider_html .= '<div id="sliderify-carousel-' . esc_attr( $instance ) . '-wrapper" class="sliderify-slider-wrapper force-image-height">';
		$slider_html .= '<div class="owl-carousel">';
		if( ! empty( $images ) ) {
			foreach( $images as $image ) {
				$image_attributes = wp_get_attachment_image_src( $image, 'large' );
			//	$slider_html .= '<div class="sliderify-owl-item-inner" data-hash="' . esc_attr( $image ) . '">';
				$slider_html .= '<div class="sliderify-owl-item-inner">';
				$slider_html .= '<img src="' . $image_attributes[0] . '">';
				$slider_html .= '</div>';
			}
		}
		$slider_html .= '</div><!-- .owl-carousel -->';
		$slider_html .= '<div class="sliderify-slider-exit sliderify-slider-close"><span></span><span></span></div>';
		$slider_html .= '</div><!-- .sliderify-slider-wrapper -->';
		
		// Apply any filters before the script
		$slider_html = apply_filters( 'sliderify_filter_slider_html', $slider_html, $images );
		
		// Get the lightbox script
		$slider_html .= sliderify_get_lightbox_slider_script( $instance );
		
		return $slider_html;
	}
}

/**
 * The parameters for our slider carousel
 * @since 1.1.0
 * @return Array
 */
if( ! function_exists( 'sliderify_get_slider_carousel_params' ) ) {
	function sliderify_get_slider_carousel_params( $items, $margin, $autowidth, $autoheight, $atts, $instance ) {
		$params = array(
			'items' 			=> absint( $items ),
			'stagePadding'		=> 0,
			'nav'				=> true,
			'navText'			=> "['<span></span><span></span>','<span></span><span></span>']",
			'center'			=> true,
			'loop'				=> true,
			'margin'			=> esc_attr( $margin ),
			'autoWidth'			=> esc_attr( $autowidth ),
			'autoHeight'		=> esc_attr( $autoheight ),
			'stageOuterClass'	=> "'sliderify-stage-outer'",
			'stageClass'		=> "'sliderify-stage'",
			'navContainerClass'	=> "'sliderify-nav'",
			'controlsClass'		=> "'sliderify-controls'",
			'dotsClass'			=> "'sliderify-dots'",
			'dotClass'			=> "'sliderify-dot'",
			'onInitialized'		=> "function(event){
				console.log('init');
			}"
		);
			
		$params = apply_filters( 'sliderify_filter_slider_carousel_params', $params, $items, $margin, $autowidth, $autoheight, $atts, $instance );
		return $params;
	}
}

/**
 * If we are displaying the lightbox slider, add a class to the body to keep it hidden and avoid nasty artifacts
 * @since 1.0.0
 * @param $classes
 * @return Array
 */
if( ! function_exists( 'sliderify_filter_body_class' ) ) {
	function sliderify_filter_body_class( $classes ) {
		if( sliderify_is_lightbox_slider_enabled() ) {
			$classes[] = 'sliderify-slider-hidden';
		}
		return $classes;
	}
}
add_filter( 'body_class', 'sliderify_filter_body_class' );

/**
 * The script for the lightbox slider
 * @since 1.0.0
 * @param $instance	Gallery ID
 * @return HTML
 */
if( ! function_exists( 'sliderify_get_lightbox_slider_script' ) ) {
	function sliderify_get_lightbox_slider_script( $instance ) {
		$script = '<script>
		jQuery(document).ready(function($){
			var scrollPos; // Ensure we return user back to same position on page
			background = $(".sliderify-slider-background");
			wrapper = $("#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper");
			carousel = $("#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper .owl-carousel");
			wrapper_height = $(window).height()*.8;
			top_height = $(window).height()*.1;
			$("#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper .sliderify-owl-item-inner,#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper,#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper .sliderify-stage-outer").css("height",wrapper_height);
			carousel.owlCarousel({
				items: 1,
				stagePadding: 64,
				nav: true,
				navText: [\'<span></span><span></span>\',\'<span></span><span></span>\'],
				center: true,
				loop: true,
				margin: 32,
				autoWidth: true,
				URLhashListener: false,
				dots: true,
		//		startPosition: \'URLHash\',
		//		stageOuterClass: \'sliderify-stage-outer\',
		//		stageClass: \'sliderify-stage\',
		//		navContainerClass: \'sliderify-nav\',
		//		controlsClass: \'sliderify-controls\',
		//		dotsClass: \'sliderify-dots\',
		//		dotClass: \'sliderify-dot\',
				onInitialized: function(){
					updatePos();
				}
			});
			function updatePos(event){
				$("body").addClass("sliderify-slider-hidden");
			}
			$("#masonry-gallery-' . esc_attr( $instance ) . ' .item").on("click",function(){
				scrollPos = $("body").scrollTop();
				$("body").scrollTop(0); // Need to return to top of page to view slider
				position = ($(this).data("pos"));
				$("#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper .owl-carousel").trigger( "to.owl.carousel", [position,300,true] );
				background.attr("style","");
				wrapper.attr("style","");
				wrapper.css("height",wrapper_height);
				$("#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper").addClass("sliderify-lightbox-activated");
				$("#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper.sliderify-lightbox-activated").css("top",top_height);
				$("body").addClass("sliderify-slider-active");
				$("body").removeClass("sliderify-slider-hidden");
			});
			$(".sliderify-slider-close").on("click",function(){
				$("body").scrollTop(scrollPos);
				background.animate({
					opacity: 0
					}, 500, function(){
					$("body").addClass("sliderify-slider-hidden");
					$("body").removeClass("sliderify-slider-active");
					$("#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper").removeClass("sliderify-lightbox-activated");
					$("#sliderify-carousel-' . esc_attr( $instance ) . '-wrapper").css("top",-9000);
				});
				wrapper.animate({ opacity: 0 }, 250 );
			});
		});
		</script>';
		$script = apply_filters( 'sliderify_filter_lightbox_slider_script', $script );
		return $script;
	}
}